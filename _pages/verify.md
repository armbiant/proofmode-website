---
title: How to Verify Proof
description: Step-by-step information on how to verify data generates by ProofMode apps
subtitle: Step-by-step information on how to verify data generates by ProofMode apps
---

Brief information on how to verify the media file, proof and signatures contained in a ProofMode ZIP file. The steps below currently only cover the INTEGRITY verification that is the first step of our [Three Layer Verification Process](https://proofmode.org/blog/three-layers).

You can learn more about the specific metadata fields available on the [Metadata Documentation](/metadata) page.

## Linux (Command Line)

### Requirements

install gpg, sha256sum, jq using your operating system package manager

### Steps

#### Import public key shared from ProofMode:

Don't know what a "Public Key" is? Learn more on this [web-based tutorial from varonis.com](https://www.varonis.com/blog/pgp-encryption)

ProofMode relies on the open standard of Open Pretty Good Privacy, or OpenPGP, for its public key cryptography. The toolkit that we recommend for verifying the digital signatures within the ProofMode zip file is [GnuPG aka gpg](https://gnupg.org), a free and open-source software package.

```
> gpg --import pubkey.asc
gpg: key xxx: public key "noone@proofmode.witness.org" imported
gpg: Total number processed: 1
gpg:               imported: 1
```

#### Check the hash of the media file against the hash in the proof metadata

A hash is another name for a "SHA256 Checksum". A [SHA-256 checksum](https://www.simplilearn.com/tutorials/cyber-security-tutorial/sha-256-algorithm) is a sequence of numbers and letters that you can use to check that your copy of a downloaded update file is identical to the original. Small changes in a file produce very different looking checksums. You can also use a checksum to help verify that an attacker has not made changes to a file.

If you know the checksum of the original file, you can run a checksum or hashing utility on it. If the resulting checksum matches, you know the file you have is identical.

```
>sha256sum YOURMEDIAFILE.jpg
THEHASHVALUEFORYOURPHOTO  YOURMEDIAFILE.jpg
```

Running the above command will give you the sha256 checksum, or "hash", that is the key to connecting the media file to the proofmode data in the proof zip. Using the THEHASHVALUEFORYOURPHOTO indicated above, you can now look for files names THEHASHVALUEFORYOURPHOTO.csv, THEHASHVALUEFORYOURPHOTO.json, and so on within the proof zip. If you can't find any files matching THEHASHVALUEFORYOURPHOTO, then the original media file was manipulated somehow.

#### Verify signature using actual photo to test GOOD signature

```
> gpg --dearmor pubkey.asc
> gpg --no-default-keyring --keyring ./pubkey.asc.gpg --homedir ./ --verify THEHASHVALUEFORYOURPHOTO.asc YOURMEDIAFILE.jpg

gpg: Signature made Thu 03 Feb 2022 10:51:16 PM EST
gpg:                using RSA key 560CC828C726365F
gpg: Good signature from "noone@proofmode.witness.org" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 094F 9BAE 1895 8C23 8F55  F793 560C C828 C726 365F
```

If you see "Good signature" as in the sample output above, then all is well! You can also see the time and date that the signature was made.

The above "WARNING" is simply saying that the key provided in the proof is not one that has been reviewed and verified by a third-party. This is normal for public keys that are generated as part of ProofMode. 

#### Verify signature of ProofMode data using actual media file to test GOOD signature

```

This command is similar to the one used for verifying the integrity of the media file, but is instead being run on the CSV proof data file.

> gpg --verify THEHASHVALUEFORYOURPHOTO.proof.csv.asc THEHASHVALUEFORYOURPHOTO.proof.csv
gpg: Signature made Thu 03 Feb 2022 10:51:18 PM EST
gpg:                using RSA key 560CC828C726365F
gpg: Good signature from "noone@proofmode.witness.org" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 094F 9BAE 1895 8C23 8F55  F793 560C C828 C726 365F
```

Again, you are looking for the "Good signature" output from the command.

#### Verify the OpenTimestamps notarization

If a .OTS file is present, you can visit [opentimestamps.org](https://opentimestamps.org) to learn more about how to upload the file to verify the notarization.
It can take awhile for the notarization timestamp to make its way fully to the Bitcoin blockchain.

You can also do command line verification using the opentimestamps-client.

```
pip3 install opentimestamps-client
ots -v v THEHASHVALUEFORYOURPHOTO.ots -f YOURMEDIAFILE.jpg
```

The output will validate the [OpenTimestamps notarization](https://petertodd.org/2016/opentimestamps-announcement) and tell you the status of its propagation onto the [Bitcoin Blockchain](https://petertodd.org/2016/opentimestamps-announcement).

As long as you see attestations from the servers associated with the Opentimestamps service, then integrity of the notarization of this proof is validated.

#### Verify the Google SafetyNet attestation

If a .GST file is present, that is a Javascript Web Token signature from Google SafetyNet notarization. You can decode the value of it for inspection at [jwt.io](https://jwt.io/).

Learn how the [SafetyNet Attestation API provides a cryptographically-signed attestation](https://developer.android.com/training/safetynet/attestation), assessing the integrity of the Android device your app is running on.

You can also decide the GST file, using the command below 

```
jq -R 'split(".") | select(length > 0) | .[0],.[1] | @base64d | fromjson' <<< cat THEHASHVALUEFORYOURPHOTO.gst
```

The output will display the values related to the attestation provided by Google's service. It includes information verifying the integrity of the device the media was captured on, as well as date and time information. 

#### Automating verification of proof data

You can find a [verify.sh script work-in-progress here](https://gitlab.com/guardianproject/proofmode/proofmode-android/-/blob/master/docs/verify.sh)

## MacOS

The same steps above from the Linux section can be completed on a MacOS computer using the Terminal app. You may need to install GPG and sha256sum tools using Homebrew or MacPorts.

On a Mac, the command is "shasum -a 256" instead "sha256sum"

[Install GPG on Mac without GPG Tools](https://mikeross.xyz/gpg-without-gpgtools-on-mac/)

You can also use any graphical or web-based PGP or Sha256 utility available on MacOS.

[GPGTools for Mac](https://gpgtools.org/)

## Windows

The same steps above from the Linux can be complete usins the [Windows Subsystem for Linux](https://ubuntu.com/wsl).

You can also use any graphical or web-based PGP or Sha256 utility available on Windows.

[GnuPG Downloads](https://www.gnupg.org/download/index.html)

[Verify SHA 256 Checksums with Windows Powershell](https://docs.precisely.com/docs/sftw/spectrum/ProductUpdateSummary/ProductUpdateSummary/source/about_sha256.html)
