---
title: ProofCorps
description: Global Verifiable Photography and Video Community
subtitle: A global pool of photographers and videographers ready to capture proof
---

## We believe in a future, where every camera will have a “Proof Mode” that can be enabled and every viewer an ability to verify-then-trust what they are seeing.

Welcome to ProofCorps. You will be using the ProofMode app to document the world around you, generating public “Proof Packs” containing photos, videos, and the extra metadata to ensure they are what they say they are. No editing, no cropping, no AI filters - we just want the pixels as captured by your smartphone’s camera. You will be assisting us by both testing the app in real world situations, and helping us build our publicly shared sets of proof from places around the world.

Imagine… there is a war, that damages an important historic site; there is a fire, that burns through an ancient forest; there is a culture that is disappearing due to modernization; there is flooding due to the ocean rising; or maybe just something wonderful that people around the world would benefit from seeing and hearing. In all of these scenarios, the world needs verifiable, authenticated photographic and video documentation before a crisis, tragedy, or change, and not after. Using ProofMode technology, the ProofCorps community will document what they see in the world around them, and freely share them with verifiable “proof” metadata, as a contribution to global society.

Here is a sample Proof Pack of a snowy walk near Boston, Massachusetts, in the United States. It is stored for preservation on the Web3.storage service: [Download Proof](https://bafybeihh4rieqwyysj7m27l4lx2e4rodgkuogxkmfnc2gp3ehidumtzfua.ipfs.w3s.link/ipfs/bafybeihh4rieqwyysj7m27l4lx2e4rodgkuogxkmfnc2gp3ehidumtzfua/proofmode-snowywalk-in-MA-2022-560cc828c726365f-2022-02-06-04-32-40.zip)

If you are interesting in participating in the ProofCorps, please contact us via [share@proofmode.org](mailto:share@proofmode.org) or find the [many other ways to contact us here](https://guardianproject.info/contact/)

## A “Proof Pack” of of a variety of interesting places on a snowy day

### Geolocation data points from the proof pack

You can see inside the zip file that not only are there JPG files of the images, but also “.proof.csv” files of metadata, .ASC digital signatures, .OTS files from third-party notarization servers, and a “HowToVerifyProofData.txt”. All of this is used to verify the place, time, source and more of the photos, so we know they match what is being presented, and that they were not otherwise edited or modified.

We recently ran a few photo scavenger hunt games in California and New York, that the concept of the ProofCorps is expanding on. You can see how we ran these events at [The Hunt](https://proofmode.org/thehunt) - we hope to run more “Hunts” in the near future with your help!

### About ProofMode


ProofMode adds extra data to photos and videos, to help people trust that it came from the person, place, and time it claims to be from. ProofMode is a system that enables authentication and verification of multimedia content, particularly captured on a smartphone, from point of capture at the source to viewing by a recipient. It utilizes enhanced sensor-driven metadata, hardware fingerprinting, cryptographic signing, and third-party notaries to enable a pseudonymous, decentralized approach to the need for chain-of-custody and “proof” by both activists and everyday people alike.


You can view a recent [public presentation on ProofMode here](https://www.beautiful.ai/player/-NHPp4pwCgIqJTMhyeBv/ProofMode-The-Hunt-Baseline-and-Beyond)

[Install ProofMode](https://proofmode.org/install)

### The Task

You will be creating “Proof Packs” (photo+video albums with extra metadata and digital signatures) of public places: historic sites, buildings, squares, parks, markets, festivals and so on. You will use the ProofMode app with the Camera app on an Android or iPhone to capture sets of roughly 10 to 20 photos and short videos of the chosen subject. You will then share them with us either using the ProofMode Cloud sharing option (iOS only for now), or a third-party file sharing (Dropbox, Drive, etc) or chat messenger service (Signal, WhatsApp, Telegram, Messages). The photos and video you are capturing will be freely licensed under a Creative Commons non-commercial license for anyone to use, and public credit for the work can be given to you if desired.


### Additional Documentation

Along with the proof zip you share, it is also helpful to include the following information in the message when you share it:

* Name / Place: the Subject of My Proof Pack: (“Hogwart’s Castle”, “Enchanted Forest”, “Christmas Faire on Briar Lane”)
Licensing:
* Acknowledgement: I acknowledge that my photos will be licensed as Creative Commons and used in articles for ProofMode:
*Byline: I want credit: Y or N
* If yes, please share the name you want to use:

### Step by Step


* Install ProofMode using the instructions here: https://proofmode.org/install
* Test the app out to make sure it is working with your phone and camera
* Open the app and enable Proof
* In Settings, enable all options: Network, Phone, Location, Notary
* Take a picture using the in app camera option
* Take a picture using your phone’s main camera app
* Share “robust” proof of a single photo using the in app options (“Share Proof”)
* Make a plan and propose a proof subject idea to the ProofCorps community manager (contact [share@proofmode.org](mailto:share@proofmode.org)) for review with an estimate of time it will take.  (After you propose a few times, we won’t need to review all ideas first.)
* Capture the proof of the subject: ideally 10-20, but more if possible, photos and short videos, in HD format - 1080p, 30fps videos in MP4 format, photos in any desired aspect ratio, ideally greater than 5MP.
* Share the media+proof using the ProofMode app either all together (multi-select on Android) or one at a time (may be necessary with larger video, or on iOS) to  share@proofmode.org 
Communicate to the community manager when you are completed capturing proof of the subject.

Go back to Step 3 and find something else interesting and worthy to capture proof of.



