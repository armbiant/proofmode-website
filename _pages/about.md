---
title: About ProofMode
description: ProofMode is an open-source project developed by Guardian Project and WITNESS
subtitle: ProofMode is an open-source project developed by Guardian Project and WITNESS
featured_image: /images/proof-feature.png
---

### We believe in a future, where every camera will have a “Proof Mode” that can be enabled and every viewer an ability to verify-then-trust what they are seeing.

ProofMode is a system that enables authentication and verification of multimedia content, particularly captured on a smartphone, from point of capture at the source to viewing by a recipient. It utilizes enhanced sensor-driven metadata, hardware fingerprinting, cryptographic signing, and third-party notaries to enable a pseudonymous, decentralized approach to the need for chain-of-custody and “proof” by both activists and everyday people alike. 

[Contact Us To Learn More](/contact)

#### How can I use ProofMode today?

ProofMode is currently available as specifications, developer libraries and applications. We seek to expand its support for notarization and sharing of proof through decentralized storage technology. 

![](/images/proof-feature.png)

ProofMode app for Android and iOS is light, minimal “reboot” of our full encrypted, verified secure camera app, formerly known as CameraV or InformaCam. We are working to build a lightweight, almost invisible utility, that you can run all of the time on your phone, that automatically adds extra digital proof data to all photos and videos you take. This data can then be easily shared through a “Share Proof” share action, to anyone you choose.

**ProofMode is free and open-source software, [available on Github](https://github.com/guardianproject/proofmode)**

This work has been done as a collaborative effort between  [Guardian Project](https://guardianproject.info), [WITNESS](https://witness.org), and [Okthanks](https://okthanks.com) for nearly a decade.

## Goals

- Run all of the time in the background without noticeable battery, storage or network impact
- Do not modify the original media files; all proof metadata storied in separate file
- Provide a no-setup-required, automatic new user experience that works without requiring training
- Use strong cryptography for strong identity and verification features, but not encryption
- Implement decentralization third-party notarization of "this media existed at this time" through cryptographic signing
- Produce “proof” data formats that can be easily parse, imported by existing tools (CSV,JSON,GeoJSON,KML)
- Support both full “proof” data generation, as well as more simple sha1/sha256 hash and PGP signature of media files
- Do not require a persistent identity or account generation
