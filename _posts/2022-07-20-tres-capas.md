---
title: 'Un Problema de Tres Capas'
date: 2022-07-20 00:00:00
featured_image: '/images/posts/tres-capas-22-07-20/image8.jpg'
excerpt: ¿Cómo checar los datos contra ellos mismos?
---

__por Jack Fox Keen__

![three peaks](/images/posts/tres-capas-22-07-20/image8.jpg)

Image by [Jasmine Co](https://www.jasminecoart.com/) (with permission and compensation)

ProofMode es un sistema que permite autentificar y verificar el contenido multimedia, en particular contenido que se capturó en un celular inteligente. Empieza desde la captura de datos por la fuente y termina con la vista del usuario. ProofMode utiliza metadatos,  huellas dactilares del hardware, las firmas criptográficas, y verificación por otros servicios externos. Todo esto permite una solución privada y descentralizada, y provee activistas y la gente en general con una vía para capturar “evidencia” de lo que ellos han visto.

¿Cómo checar los datos contra ellos mismos? Nuestra respuesta involucra tres capas de análisis.

- Integridad
- Consistencia
- Sincronia

**La primera capa es Integridad vía encriptación.** ProofMode usa criptografía para firmar cada archivo de media y cada archivo de datos de ProofMode. Esto incluye valores de hash de los archivos de media de los datos de la evidencia. Estos se llaman firmas digitales. Este valor se puede compartir con servicios externos. Este “hash” se puede enviar privadamente a través de una aplicación de mensajería o correo electrónico. También, puedes publicar en servicios como Twitter o Facebook. Estos métodos establecen que este conjunto de bytes fue creado al mismo tiempo que el valor de “hash” fue compartido o fue publicado.

Cuando pensamos en términos de analizar integrado, nos podemos hacer las siguientes preguntas:
¿La llave para firmar los datos coincide con la persona que envió los medios audiovisuales y la evidencia?
En otras palabras, ¿los archivos de medios y el contenido de la evidencia no fueron alterados?

**La segunda capa es Consistencia.** ¿Qué significa para los datos ser consistente internamente? Aquí es donde checamos si los datos se contradicen entre ellos. Checamos solamente los datos contra sí mismos, sin aporte externo. Por ejemplo, si recibimos datos de evidencia, como podemos checar consistencia interna de las fotos? ¿Qué estamos buscando?

Un ejemplo principal de este proceso es buscar consistencia usando metadatos. Por ejemplo, la información del celular, incluyendo la información de la red sobre la localización del dispositivo cuando se capturó la imagen. Si alguien entrega evidencia, nuestro programa de consistencia interna hace un análisis sencillo, generando un histograma en capas para el número de los atributos para cada dispositivo único.

![histogram](/images/posts/tres-capas-22-07-20/image1.png)

En este histograma, podemos ver que hay tres dispositivos que no tienen información para el proveedor de ubicación. Pueden ver esto en la barra verde oscura, donde 0 está en el eje x.


![histogram](/images/posts/tres-capas-22-07-20/image5.png)

Si miras en el eje x, sobre el 2, hay un dispositivo que tiene dos torres de celulares asociadas con las fotos. Esto puede implicar, por ejemplo, que el usuario caminó una mayor distancia que los otros usuarios, y entonces fue expuesto a más torres de celulares mientras tomaba las fotos.

![histogram](/images/posts/tres-capas-22-07-20/image7.png)

Es importante enfatizar que ProofMode ofrece al usuario autonomía total sobre cuáles datos son recolectados de la aplicación y cuales datos son compartidos. Usuarios pueden seleccionar las configuraciones de los datos, como ubicación y la información de los celulares, por ejemplo. Nuestra meta es modelar todas las combinaciones posibles de los datos recolectados. Vamos a medir como estas combinaciones contribuyen para medir la fuerza de la evidencia asociada con cada foto.

![histogram](/images/posts/tres-capas-22-07-20/image6.png)

**Y finalmente, tenemos nuestro análisis de sincronía.** Aquí empezamos incorporando la información externa y vemos como los datos corresponden con las observaciones del evento. ¿Coinciden los datos con la calidad, frecuencia, escala, y alcance del evento documentado?

Por ejemplo, con los datos de ubicación, nos hemos enfocado en la “forma” o topología del evento. Hacemos esto quitando información sensible, como las coordenadas de longitud y latitud, y enfocamos en el casco convexo de las fotos. El casco convexo es una forma sencilla que contiene todos los puntos de ubicación. Esta forma, o polígono, puede ser comparado a los límites del evento. Por ejemplo, nosotros esperaríamos que fotos de una protesta en Times Square tengan la forma de un cuadro. Igualmente, esperaríamos que las fotos de una marcha coincidieran con la forma de las calles que se caminaron.

Durante nuestra evaluación de estas capas–integridad, consistencia, y sincronía–tenemos más preguntas. ¿Cómo sintetizamos y resumimos el análisis de las capas? Integridad es una evaluación binaria–las llaves coinciden o no. Consistencia es una evaluación cuantitativa–estamos mirando al número de celulares en un evento y el número de atributos asociados con cada celular. Sincronía es una evaluación cualitativa–la forma del evento no es un número o “si o no” respuesta. ¿Cual es la mejor y más intuitiva vía para resumir estos tipos de información muy diferentes?

Nuestro primer intento para resumir los datos es una gráfica de radar. Cada capa de evaluación puede ser evaluada para categorías diferentes de los datos, como hardware y software.


![histogram](/images/posts/tres-capas-22-07-20/image4.png)

Un detalle importante es que cada capa naturalmente se construye sobre la capa anterior.

Si la evidencia no pasa el análisis de integridad, evaluando que las firmas criptográficas coinciden o no, los datos no son fidedignos. Esta es la fundación de ProofMode. Sin esta fundación, no se puede construir.

![house](/images/posts/tres-capas-22-07-20/image9.png)
Original image by Wikimedia Commons

Si la evidencia pierde puntos con el análisis de consistencia interna, como muchos países asociados con un celular por un evento, posiblemente tus datos no son de confianza. Esto es como los muros de la evidencia. Queremos que nuestra evidencia esté entre los muros de los datos. 

![house](/images/posts/tres-capas-22-07-20/image2.png)  
 Original image by Wikimedia Commons

Si la evidencia no coincide con el análisis de sincronía externa, como la forma de las fotos es diferente que la topología del evento, posiblemente tus datos son  inconsistentes. Esto es como el cielorraso y techo de la evidencia. Unimos todo en una estructura cohesiva.  Original image by Wikimedia Commons

![house](/images/posts/tres-capas-22-07-20/image3.png)  
 Original image by Wikimedia Commons

ProofMode proporciona garantía fuerte de la veracidad de lo que ves. Tenemos la captura segura de los datos con los medios audiovisuales. Tenemos el intercambio seguro y privado de metadatos con contactos confiables y defensores. Tenemos un sistema para verificación profunda a través de tres capas.

ProofMode está disponible actualmente como especificaciones, bibliotecas de desarrollo, y aplicaciones. Estamos trabajando para expandir apoyo para notarización, almacenamiento e intercambio de evidencia a través de tecnología descentralizada, además de expandir sus funciones a más dispositivos y aplicaciones.

Nuestra meta es que cada celular inteligente sea empoderado con verificación criptográfica de las imágenes para combatir la desinformación. Estamos trabajando para proveer software de código abierto y libre, con otras organizaciones.



