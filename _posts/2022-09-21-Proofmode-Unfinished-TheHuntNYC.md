---
title: 'THE HUNT NYC ProofMode Game at Unfinished Live! NYC'
date: 2022-09-21 10:00:00
featured_image: '/images/thehunt-promo-nyc.jpg'
excerpt: Use the ProofMode app to play The Hunt NYC, a 24-hour disinformation-fighting scavenger hunt where the motto is “cryptographically verified pics or it didn’t happen!”
---

We are excited to be attending [Unfinished Live 2022](https://live.unfinished.com/) in New York this week, alongside our partners the [Filecoin Foundation for the Decentralized Web](https://www.ffdweb.org/guardian-project-annoucement/) and [WITNESS](https://witness.org).

We will be running the latest incarnation of our ProofMode-based game [THE HUNT](/thehunt), a way to learn about how ProofMode works while having a whole lot of fun: 

*Use the ProofMode app to play The Hunt NYC, a 24-hour disinformation-fighting scavenger hunt where the motto is “cryptographically verified pics or it didn’t happen!” Take photos of up to six required items commonly found around NYC in a 24-hour period, share the “robust” proof through [Signal App](https://signal.org) to [+1 718 569 7272](sms:17185697272) or email [hunt@proofmode.org](mailto:hunt@proofmode.org), and win amazing prizes!*

![](/images/thehunt-promo-nyc.jpg)

Guardian Project and FFDWeb team members will be in person at Unfinished Live handing out game cards, walking through the setup of the ProofMode app, and otherwise helping anyone interested to JOIN THE HUNT! 

Here are some details on how the game will be played and verified:

- To play, you must [install the ProofMode app for iOS or Android](/install)
- The top players who submit the most verified proof within the required time frame and location will be the winners.
- You don't have to be at Unfinished to play the game, but you do have to be in NYC.
- All proof must be captured between **12PM on Thursday, September 22nd, and 12PM on Friday, September 23rd**.
- Winners will be notified on Friday, after all submitted proof is verified using our [ProofCheck process and tools](/verify)
- If you have any issues or other questions, please message us on Signal at [+1 718 569 7272](sms:17185697272), or email [hunt@proofmode.org](mailto:hunt@proofmode.org)

![](/images/thehunt-card-nyc.jpg)



