---
title: 'Fall update on our roadmap, architecture and vision for a more verified world'
date: 2022-11-21 10:00:00
featured_image: '/images/proofmode-roadmap-fall-2022.jpg'
excerpt: A great deal of technical and planning work is underway to move towards our next-generation ProofMode launch
---

View a recent presentation we shared with our community below or [at this link](https://www.beautiful.ai/player/-NHPp4pwCgIqJTMhyeBv)

As always, there are [many ways to get in touch](/contact) if you are interested in learning more, getting help using ProofMode, or partnering with us. 
<hr/>
<div style="position:relative;width:100%;height:0;padding-bottom:calc(56.25% + 40px);"><iframe allowfullscreen style="position:absolute; width: 100%; height: 100%;border: solid 1px #333;" src="https://www.beautiful.ai/embed/-NHPtPym-3-DNUZfs-Bz?utm_source=beautiful_player&utm_medium=embed&utm_campaign=-NDUKLz5VN2xTnzUsx97"></iframe><a href="https://www.beautiful.ai/embed/-NHPtPym-3-DNUZfs-Bz?utm_source=beautiful_player&utm_medium=embed&utm_campaign=-NDUKLz5VN2xTnzUsx97">View ProofMode: The Hunt, Baseline and Beyond on Beautiful.ai</a></div>


