---
title: 'Presentation on ProofMode'
date: 2021-12-08 00:00:00
featured_image: '/images/proofmode1.png'
excerpt: A solid overview of the ProofMode concept and project
---

The presentation linked below gives a solid overview of the ProofMode concept, progress, applications, data formats, and current implementing partners.

View the slides on our Github project: [View Slides](https://github.com/guardianproject/proofmode/blob/master/docs/ProofMode2020.pdf)

[![ProofMode Slides](/images/proofmode1.png)](https://github.com/guardianproject/proofmode/blob/master/docs/ProofMode2020.pdf)



