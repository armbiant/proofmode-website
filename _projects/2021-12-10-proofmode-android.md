---
title: 'ProofMode for Android'
subtitle: 'Free and open-source Android app'
date: 2021-12-10 00:00:00
##featured_image: '/images/screens/proofmode1.png'
excerpt: Learn where you can find our Android app and how to contribute to its development.
---

ProofMode for Android is now available on <a href="https://play.google.com/store/apps/details?id=org.witness.proofmode">Google Play</a>, <a href="https://guardianproject.info/fdroid">F-Droid</a> and <a href="https://github.com/guardianproject/proofmode/releases">Github for direct download</a>.

Ongoing development is done on our [public Github project](https://github.com/guardianproject/proofmode/).

<div class="gallery" data-columns="3">
	<img src="/images/screens/proofmode1.png">
	<img src="/images/screens/proofmode3.png">
	<img src="/images/screens/proofmode4.png">
</div>
