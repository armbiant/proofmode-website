---
title: 'ProofMode Developer Libraries'
subtitle: 'Code and libraries for developers to integrate ProofMode'
date: 2021-12-10 00:00:00
##featured_image: '/images/screens/proofmode1.png'
excerpt: Learn how you can add ProofMode capability to your apps and services.
---

ProofMode developed library for Android is available today, with the iOS and other platform libraries available soon.

You can access the source code for the Android library within the main project at [https://github.com/guardianproject/proofmode/tree/master/android-libproofmode](https://github.com/guardianproject/proofmode/tree/master/android-libproofmode).

The library is available as a maven dependency on Jitpack.io

	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}

Then, in build.grade dependencies add:

	dependencies {
	        implementation 'com.github.guardianproject:proofmode:0.10-library'
	}

Example implementation of how to generate ProofMode metadata within your application is available through the OpenArchive Save for Android app.

You can see the [call to "generateProof()" here](https://github.com/OpenArchive/Save-app-android/blob/master/app/src/main/java/net/opendasharchive/openarchive/services/webdav/WebDAVSiteController.kt#L417).
